import React, { useState } from "react";
import { StyleSheet, Text, ToastAndroid, TouchableOpacity, View } from "react-native";

const App = () => {

  const [digit,setDigit] = useState("0");
  const [digitF,setDigitF] = useState(0);
  const [calcType,setCalc] = useState(null);
  const [flag,setFlag] = useState(0);
  const showToast = () => {
      ToastAndroid.show("Can't divide by 0!", ToastAndroid.SHORT);
    };
  let digit3 = 0;

  function setContainer(p1){
    if(p1 == "."){
      if(digit.includes(".") == false){
        setDigit(digit + ".");
      }
    }
    else{
      if(digit == "0"){
        setDigit(p1)
      }else {
        setDigit(digit + p1)
      }
    }
  }

  function operation(p1){
    if(flag == 0){
      const math = p1;
      setCalc(math);
      const digit1 = parseFloat(digit);
      setDigitF(digit1);
      setDigit("0");
      setFlag(1);
    }
  }

  function equaition(){
    const digitS = parseFloat(digit);
    if(flag == 1){
      switch (calcType) {
        case '+':
          digit3 = (digitF + digitS).toFixed(2);
          setDigit(digit3);
          setCalc('=');
        break;
        case '-':
          digit3 = (digitF - digitS).toFixed(2);
          setDigit(digit3);
          setCalc('=');
        break;
        case '*':
          digit3 = (digitF * digitS).toFixed(2);
          setDigit(digit3);
          setCalc('=');
        break;
        case '/':
          if(digitS == 0){
            showToast();
          }
          else{
            digit3 = (digitF / digitS).toFixed(2);
            setDigit(digit3);
            setCalc('=');
          }
        break;
      }
      setFlag(0);
    }
  }

  const onPress1 = () => setContainer("1");
  const onPress2 = () => setContainer("2");
  const onPress3 = () => setContainer("3");
  const onPress4 = () => setContainer("4");
  const onPress5 = () => setContainer("5");
  const onPress6 = () => setContainer("6");
  const onPress7 = () => setContainer("7");
  const onPress8 = () => setContainer("8");
  const onPress9 = () => setContainer("9");
  const onPress0 = () => setContainer("0");
  const onPressPoint = () => setContainer(".");
  const onPressAC = () => setDigit("0");
  const onPressAdd = () => operation('+');
  const onPressMin = () => operation('-');
  const onPressMulti = () => operation('*');
  const onPressDiv = () => operation('/');
  const onPressEq = () => equaition();

  return (
    <View style={styles.container}>
      <View style={styles.result}>
        <Text style={styles.resultText}>{digit}</Text>
      </View>
      <View style={styles.buttonsContainer}>
        <TouchableOpacity style={styles.buttons} onPress={onPressAC}>
          <Text style={styles.buttonText}>AC</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons0}>
          <Text style={styles.buttonText}></Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight} onPress={onPressDiv}>
          <Text style={styles.buttonText}>/</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress7}>
          <Text style={styles.buttonText}>7</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress8}>
          <Text style={styles.buttonText}>8</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress9}>
          <Text style={styles.buttonText}>9</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight} onPress={onPressMulti}>
          <Text style={styles.buttonText}>X</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress4}>
          <Text style={styles.buttonText}>4</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress5}>
          <Text style={styles.buttonText}>5</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress6}>
          <Text style={styles.buttonText}>6</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight} onPress={onPressMin}>
          <Text style={styles.buttonText}>-</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress1}>
          <Text style={styles.buttonText}>1</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress2}>
          <Text style={styles.buttonText}>2</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPress3}>
          <Text style={styles.buttonText}>3</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight} onPress={onPressAdd}>
          <Text style={styles.buttonText}>+</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons0} onPress={onPress0}>
          <Text style={styles.buttonText}>0</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttons} onPress={onPressPoint}>
          <Text style={styles.buttonText}>,</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.buttonsRight} onPress={onPressEq}>
          <Text style={styles.buttonText}>=</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    display:"flex",
    flexWrap:"wrap",
    flex: 1,
    alignItems:"flex-start",
    justifyContent:"flex-start",
    backgroundColor:"#666666"
  },
  result:{
    display: "flex",
    height: "30%",
    width: "98.5%",
    marginHorizontal:3,
    backgroundColor:"#b36b00",
    justifyContent: "center",
    alignItems: "flex-end"
  },
  resultText:{
    display: "flex",
    alignSelf:"flex-end",
    fontSize: 100,
    color: "#ffffff"
  },
  buttonsContainer:{
    display: "flex",
    flex:1,
    display:"flex",
    flexWrap:"wrap",
    flexDirection:"row",
    padding:1,
  },
  buttons:{
    display:"flex",
    height: "19.6%",
    width: "24.5%",
    backgroundColor:"#000000",
    margin: 1,
    alignItems: "center",
    justifyContent:"center"
  },
  buttonText:{
    display: "flex",
    alignSelf:"center",
    fontSize: 28,
    color: "#ffffff"
  },
  buttons0:{
    display:"flex",
    height: "19.6%",
    width: "49.4%",
    backgroundColor:"#000000",
    margin: 1,
    flexDirection: "row",
    alignItems: "flex-start",
    paddingHorizontal: 43
  },
  buttonsRight:{
    display:"flex",
    height: "19.6%",
    width: "24.5%",
    backgroundColor:"#000000",
    margin: 1,
    alignItems: "center",
    justifyContent:"center",
    backgroundColor:"#ff8c1a"
  }
});

export default App;
